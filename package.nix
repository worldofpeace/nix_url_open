{ lib
, beamPackages
, makeWrapper
, nixUnstable
, glib
}:

let

  jason = beamPackages.buildMix {
    name = "jason";
    version = "1.4.0";

    src = beamPackages.fetchHex {
      pkg = "jason";
      version = "1.4.0";
      sha256 = "sha256-eaN5EIWyoPdDygTOwPe+JkQ3OHedCTAuATGPl724ISE=";
    };
  };

in

beamPackages.buildMix {
  name = "nix-url-open";
  version = "0.1.0";

  src = lib.cleanSource ./.;

  nativeBuildInputs = [
    makeWrapper
  ];

  beamDeps = with beamPackages; [ erlang jason ];

  buildPhase = ''
    export HEX_OFFLINE=1
    export HEX_HOME=`pwd`
    export MIX_ENV=prod
    export MIX_NO_DEPS=1

    mix escript.build --no-deps-check
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp nix_url_open $out/bin
  '';

  postFixup = ''
    wrapProgram $out/bin/nix_url_open \
      --prefix PATH : "${lib.makeBinPath [ nixUnstable glib.bin ]}"
  '';
}
