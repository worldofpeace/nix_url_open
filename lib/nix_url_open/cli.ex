defmodule NixUrlOpen.CLI do
  alias NixUrlOpen.{Nix, Gio}

  def help do
    IO.puts("""
    Use `gio open` to open the homepages to Nix packages.

    Arguments the script accepts:
    <attribute_paths>
    -f, --file path to local nixpkgs
    -h, --help show help menu

    Examples:
    nix_url_open "notes-up"
    nix_url_open "lollypop" "gnome-photos" -f "../nixpkgs"
    """)

    :help
  end

  def main(args) do
    parse_args(args)
    |> run()
  end

  def run(:help), do: help()

  def run({nixpkgs_path, attributes}) do
    Nix.get_homepages!(attributes, List.to_string(nixpkgs_path))
    |> Gio.gio_backend().open_urls!()
  end

  def run(attributes) when attributes != [] do
    Nix.get_homepages!(attributes)
    |> Gio.gio_backend().open_urls!()
  end

  def run([]), do: run(:help)

  def parse_args(args) do
    {options, attributes} =
      OptionParser.parse!(args,
        strict: [
          file: :string,
          help: :boolean
        ],
        aliases: [
          f: :file,
          h: :help
        ]
      )

    parse_options(options, attributes)
  end

  defp parse_options([file: path], attributes),
    do: {[Path.expand(path)], attributes}

  defp parse_options([help: true], _),
    do: :help

  defp parse_options(_, attributes),
    do: attributes
end
