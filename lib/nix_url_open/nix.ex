defmodule NixUrlOpen.Nix do
  def executable do
    nix = System.find_executable("nix")

    if nix == nil do
      raise RuntimeError, message: "missing executable for 'nix'"
    end

    nix
  end

  def eval!(attribute) do
    System.cmd(
      executable(),
      [
        "eval",
        "--json",
        # flake uri fragment
        "nixpkgs#" <> attribute
      ]
    )
    |> handle_eval()
  end

  def eval!(attribute, nixpkgs_path) do
    System.cmd(
      executable(),
      [
        "eval",
        "--json",
        "-f",
        nixpkgs_path,
        attribute
      ]
    )
    |> handle_eval()
  end

  defp handle_eval({eval_result, 0}) do
    case eval_result do
      "" -> eval_result
      _ -> Jason.decode!(eval_result, %{})
    end
  end

  defp handle_eval({eval_result, _}) do
    raise RuntimeError, message: "Error running nix eval: #{eval_result}"
  end

  defp homepage_attribute(attribute),
    do: attribute <> ".meta.homepage"

  def get_homepages!(attributes, nixpkgs_path) do
    Enum.map(attributes, fn attribute ->
      eval!(homepage_attribute(attribute), nixpkgs_path)
    end)
  end

  def get_homepages!(attributes) do
    Enum.map(attributes, fn attribute ->
      eval!(homepage_attribute(attribute))
    end)
  end
end
