defmodule NixUrlOpen.Gio do
  def executable() do
    gio = System.find_executable("gio")

    if gio == nil do
      raise RuntimeError, message: "missing executable for 'gio'"
    end

    gio
  end

  def gio_backend() do
    Application.get_env(:nix_url_open, :gio_executable)
  end

  def open_url!(uri) do
    System.cmd(
      executable(),
      [
        "open",
        uri
      ]
    )
  end

  def open_urls!(urls) do
    Enum.each(urls, fn url ->
      open_url!(url)
    end)
  end
end
