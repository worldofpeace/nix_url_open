# NixUrlOpen
Use `gio open` to open the homepages to Nix packages.

Arguments the script accepts:
<attribute_paths>
-f, --file path to local nixpkgs
-h, --help show help menu

Examples:
```sh
nix_url_open "notes-up"
nix_url_open "lollypop" "gnome-photos" -f "../nixpkgs"
```

## Installation
This package uses the experimental Nix Flakes interface (nixUnstable).

### For Flakes

To run the package

```sh
nix run "git+https://codeberg.org/worldofpeace/nix_url_open?ref=main" ...
```

A shell with the package available
```sh
nix shell "git+https://codeberg.org/worldofpeace/nix_url_open?ref=main"
```

Ad-Hoc install
```sh
nix profile install "git+https://codeberg.org/worldofpeace/nix_url_open?ref=main"
```

or if you're using NixOS you could use the module
```nix
# This is an example flake.nix for a NixOS system

{
  inputs.nix-url-open = {
    url = "https://codeberg.org/worldofpeace/nix_url_open";
    type = "git";
    ref = "main";
  };

  outputs = { self, nixpkgs, nix-url-open }: {
     # replace 'joes-desktop' with your hostname here.
     nixosConfigurations.joes-desktop = nixpkgs.lib.nixosSystem {
       system = "x86_64-linux";

       # Pass flake ‘inputs’ as an argument to all modules.
       specialArgs = {
        inherit inputs;
       };

       modules = [ ./configuration.nix nix-url-open.nixosModule  ];
     };
  };
}
```

and add `nix-url-open` to `environment.systemPackages` in your `configuration.nix`.
The module only extends pkgs with the overlay.

### Legacy
This project uses `flake-compat` for legacy nix.

To install:
```
nix-env -f https://github.com/worldofpeace/nix_url_open/archive/master.tar.gz -iA default
```

## Dev
Use `nix develop` if using `nixUnstable`. Otherwise this is still compatible with `nix-shell`.
There's also `direnv` support which requires [`nix-direnv`](https://github.com/nix-community/nix-direnv).

