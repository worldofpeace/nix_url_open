ExUnit.start()

defmodule ExUnit.InitialContext do
  def setup_context(_context) do
    %{
      # a nix attribute cannot be numbers
      fake_attr: "12345",
      hello: "hello",
      hello_homepage: "https://www.gnu.org/software/hello/manual/",
      gnused: "gnused",
      gnused_homepage: "https://www.gnu.org/software/sed/",
      git: "git",
      git_homepage: "https://git-scm.com/",
      fake_file: "/nixpkgs",
      fake_flag: "--lol"
    }
  end
end
