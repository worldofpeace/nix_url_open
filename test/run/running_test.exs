Code.require_file("../gio.exs", __DIR__)

defmodule NixUrlOpen.Test.RunningTest do
  use ExUnit.Case

  alias NixUrlOpen.{CLI, Nix}

  defp check_for_programs(_context) do
    try do
      NixUrlOpen.Nix.executable()
    rescue
      RuntimeError -> Mix.raise("You need nix installed to run these tests.")
    end

    :ok
  end

  import ExUnit.InitialContext
  setup_all [:setup_context, :check_for_programs]

  test "retrive one homepage from local expression", context do
    assert Nix.get_homepages!([context.hello], __DIR__) == [context.hello_homepage]
  end

  test "retrive multiple homepages from local expression", context do
    assert Nix.get_homepages!(
             [
               context.hello,
               context.gnused,
               context.git
             ],
             __DIR__
           ) == [
             context.hello_homepage,
             context.gnused_homepage,
             context.git_homepage
           ]
  end

  test "retrive one homepage using nixpkgs flake", context do
    assert Nix.get_homepages!([context.hello]) == [context.hello_homepage]
  end

  test "retrive mulitple homepages using nixpkgs flake", context do
    assert Nix.get_homepages!([
             context.hello,
             context.gnused,
             context.git
           ]) == [
             context.hello_homepage,
             context.gnused_homepage,
             context.git_homepage
           ]
  end

  describe "run the program via the entrypoint" do
    test "if a help flag is present in args it should always show help", context do
      assert CLI.main([]) == :help
      assert CLI.main(["--help"]) == :help
      assert CLI.main(["--help", context.hello]) == :help
      assert CLI.main(["--help", context.hello, context.gnused, context.git]) == :help

      # assert CLI.main(["--help", context.hello, context.gnused, context.git, "--file", __DIR__]) ==
      #          :help
    end

    test "some basic attributes", context do
      assert CLI.main([context.hello]) == {:ok, [context.hello_homepage]}

      assert CLI.main([context.hello, context.gnused, context.git]) ==
               {:ok, [context.hello_homepage, context.gnused_homepage, context.git_homepage]}
    end
  end
end
