{
  hello = {
    meta = {
      homepage = "https://www.gnu.org/software/hello/manual/";
    };
  };

  gnused = {
    meta = {
      homepage = "https://www.gnu.org/software/sed/";
    };
  };

  git = {
    meta = {
      homepage = "https://git-scm.com/";
    };
  };
}

