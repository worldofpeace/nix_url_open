defmodule NixUrlOpen.Test.CLI do
  use ExUnit.Case, async: true
  doctest NixUrlOpen.CLI

  import ExUnit.InitialContext
  setup_all :setup_context

  import NixUrlOpen.CLI, only: [parse_args: 1]

  describe "check that parse_args returns expected values" do
    test ":help with -h and --help switches", context do
      assert parse_args(["-h", context.fake_attr]) == :help
      assert parse_args(["--help", context.fake_attr]) == :help
    end

    test "passed arguments should be returned", context do
      assert parse_args([context.fake_attr]) == [context.fake_attr]
    end

    test ":file with -f and --file switches", context do
      assert parse_args([context.fake_attr, "-f", context.fake_file]) ==
               {[context.fake_file], [context.fake_attr]}

      assert parse_args([context.fake_attr, "--file", context.fake_file]) ==
               {[context.fake_file], [context.fake_attr]}
    end

    test "give me nothing" do
      assert parse_args([]) == []
    end

    test "should error on fake flags", context do
      catch_error(parse_args([context.fake_flag, context.fake_attr]) == 1)
      catch_error(assert parse_args([context.fake_flag]) == 1)
    end
  end
end
