defmodule NixUrlOpen.Test.Gio do
  @doc """
  Stub for the Gio module that does nothing.
  """
  def open_urls!(urls), do: {:ok, urls}
end
