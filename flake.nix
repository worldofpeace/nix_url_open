{
  description = "nix_url_open - Use `gio open` to open the homepages to Nix packages";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  outputs = {self, nixpkgs, flake-utils, ... }:
    {
      nixosModule = { pkgs, ... }: {
        nixpkgs.overlays = [
          self.overlay
        ];
      };

      overlay = final: prev: {
        nix-url-open = final.pkgs.callPackage ./package.nix {};
      };
    }
    //
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages.nix-url-open = pkgs.callPackage ./package.nix {};

        defaultPackage = packages.nix-url-open;

        apps.nix-url-open = flake-utils.lib.mkApp {
          drv = packages.nix-url-open;
          exePath = "/bin/nix_url_open";
        };

        defaultApp = apps.nix-url-open;

        devShell = pkgs.mkShell {
          name = "nix-url-open-shell";

          buildInputs = with pkgs; [
            elixir
            erlang
            common-updater-scripts
          ]
          ++ pkgs.lib.optional pkgs.stdenv.isLinux pkgs.libnotify # For ExUnit Notifier on Linux.
          ++ pkgs.lib.optional pkgs.stdenv.isLinux pkgs.inotify-tools # For file_system on Linux.
          ;
        };
      });
}
